import registrarApp1 from './1-dashboard.jpg';
import registrarApp2 from './2-instructors.jpg';
import registrarApp3 from './3-students-list.jpg';
import registrarApp4 from './4-courses-dashboard-overview.jpg';
import registrarApp5 from './5-exams-current-year.jpg';
import registrarApp6 from './6-study-programs-active.jpg';
import registrarApp7 from './7-theses.jpg';
import registrarApp8 from './8-requests.jpg';
import registrarApp9 from './9-registrations.jpg';
import registrarApp10 from './10-students-graduated.jpg';

export default {
  registrarApp1,
  registrarApp2,
  registrarApp3,
  registrarApp4,
  registrarApp5,
  registrarApp6,
  registrarApp7,
  registrarApp8,
  registrarApp9,
  registrarApp10
}
