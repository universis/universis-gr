import React, { useState } from 'react';
import ReactDOM from 'react-dom'
import Slider from 'react-slick';
import { useTranslation } from 'react-i18next';
import { images } from './../../../images/images';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Translation } from 'react-i18next'

export class FullScreenCarousel extends React.PureComponent{
  constructor(props) {
    super(props);
    this.appRoot = document.getElementById('gatsby-focus-wrapper');
    this.el = document.createElement('div');
    this.el.setAttribute('class', 'js-slick-full-screen-wrapper');
  }

  componentDidMount() {
    this.appRoot.appendChild(this.el);
    document.getElementsByTagName('HTML')[0].style.overflowY = "hidden";
  }

  componentWillUnmount() {
    this.appRoot.removeChild(this.el);
    document.getElementsByTagName('HTML')[0].style.overflowY = "scroll";
  }

  render() {
    const carouselModal = (
      <>
        <div className="carousel-fullscreen__header text-right">
          <span className="carousel-fullscreen__close-button">
            <button className="btn btn--secondary type--uppercase pl-4 pr-4 mr-4" onClick={this.props.handleClick}>
              <Translation>
                {
                  (t) => <span> {t('Generic.Close')} </span>
                }
              </Translation>
            </button>
          </span>
        </div>
        <div className="js-slick-full-screen-wrapper--inner">
          <Carousel
            title={this.props.title}
            images={this.props.images}
            currentIndex={this.props.currentIndex}
            isFullScreen
          />
        </div>
      </>
    )

    return ReactDOM.createPortal(
      carouselModal,
      this.el
    );
  }
}

export class Carousel extends React.PureComponent {

  constructor(props) {
    super(props);
    this.isFullScreen = this.props.isFullScreen;
    this.ref = React.createRef();
    this.settings = {
      dots: !this.isFullScreen,
      infinite: !this.isFullScreen,
      speed: 300,
      slidesToShow: this.isFullScreen ? 1 : 3,
      centerPadding: '60px',
      slidesToScroll: 1,
      arrows: true,
      centerMode: true,
      responsive: [
        {
          breakpoint: 1100,
          settings: {
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: this.isFullScreen ? 1 : 2,
            arrows: true,
          }
        },
        {
          breakpoint: 770,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1,
            dots: false
          }
        }
      ]
    };
  }

  componentDidMount() {
    if (this.props.currentIndex && this.props.currentIndex > 0 && this.ref && this.ref.slickGoTo) {
      this.ref.slickGoTo(this.props.currentIndex, true);
    }
  }

  handleClick = this.props.handleClick
    ? (index) => this.props.handleClick(index)
    : () => { };

  render() {
    const carouselItems = this.props.images.map((item, i) => {
      return (
        <div
          key={i}
          role="button"
          onClick={() => {this.handleClick(i)}}
          tabIndex={0}
          onKeyDown={
            (e) => {
              if (e.keyCode === 13 || e.keyCode === 32) {
                this.handleClick(i);
              }
            }
          }

        >
          <img src={images[item.imageName]} alt={item.imageAlt} />
        </div>
      );
    });

    return (
      <Slider ref={ref=>{ this.ref=ref}} {...this.settings} >
        {carouselItems}
      </Slider>
    );
  }
}


export const  CarouselWrapper = props => {
  const [isOpen, setIsOpen] = useState(false);
  const [childIndex, setChildeIndex] = useState(0);
  const { t } = useTranslation();

  const canExpand = () => (
    typeof window !== 'undefined'
    && window.innerWidth >= 700
    && props.allowFullScreen
  )

  return (
    <div className="js-slick-wrapper col">
      <p className="lead text-center">
        <strong> {t(props.title)} </strong>
      </p>
      <div>
        <Carousel
          images={props.images}
          handleClick={(index) => {
            setChildeIndex(index);
            setIsOpen(!isOpen);
          }}
        />
      </div>
      {
        canExpand()
        && isOpen
        && (
          <FullScreenCarousel
            images={props.images}
            currentIndex={childIndex}
            handleClick={() => {(isOpen || canExpand()) && setIsOpen(!isOpen)}}
          />
        )

      }
    </div>
  );
}

export default CarouselWrapper;
