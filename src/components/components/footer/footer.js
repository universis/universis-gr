import React from 'react';
import { useTranslation } from 'react-i18next';

const Footer = props => {
  const { t } = useTranslation();
  return (
    <footer className="footer-3 text-center-xs space--xs ">
      <div className="container">
        <div className="row" />
        <div className="row">
          <div className="col-md-6">
            <p className="type--fine-print">
              {t(props.leftText)}
            </p>
          </div>
          <div className="col-md-6 text-right text-center-xs">
          {
            props.copyright &&
            <span className="type--fine-print">
              &copy; <span className="update-year" /> {props.copyright}
            </span>          
          }  
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
