import React from 'react';
import { useTranslation } from 'react-i18next';

/**
 *
 * Presents a single partner item
 *
 */
export const News = props => {
    return (
        <div className="card col-md-10 justify-content-center m-auto p-auto" id={props.id}>
            <div className="feature feature-8">
                <h3 className="card-title text-center ml-3">{props.title}</h3>
                <br/>
                <div className="card-body h4 mb-0" dangerouslySetInnerHTML={{__html: props.body}}></div>
            </div>
        </div>
    );
}

/**
 *
 * Holds the list of partners
 * It is expected to be used inside a PageSection component
 *
 */
export const NewsList = props => {
    const { t } = useTranslation();
    const news = props.news.map((item, i, arr) => (
        <News
            key={i}
            id={item.id}
            title={t(item.title)}
            body={t(item.body)}
        />
    ));

    return (
        <>
            {news}
        </>
    );
}

export default NewsList;
