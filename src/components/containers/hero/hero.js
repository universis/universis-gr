import React from 'react';
import { graphql } from 'gatsby';
import uniStudent from "../../../assets/images/drawkit-content-man-colour.svg";
import { useTranslation } from 'react-i18next';

const Hero = props => {
  const { t } = useTranslation();
  
  const button = props.button && props.button.href && props.button.text 
    ? (
      <a className="btn btn--primary type--uppercase" href={props.button.href}>
        <span className="btn__text">{t(props.button.text)}</span>
      </a>
      )
    : undefined;
    
  return (
    <section>
      <div className="container">
        <div className="row align-items-center justify-content-around">
          <div className="col-md-6 col-lg-5">
            <h1 className="head-title">{t(props.title)}</h1>
            <h2>{t(props.subtitle)}</h2>
            <p className="lead">{t(props.body)}</p>
            { button }
          </div>
          <div className="col-md-6 col-sm-4 text-left">
            <img alt="This is person who is presenting a browser" src={uniStudent} />
          </div>
        </div>
      </div>
    </section>
  );
}
export default Hero;

export const query = graphql`
  fragment HeroData on ContentJsonHero {
    title
    subtitle
    body,
    button {
      text
      href
    }
  }
`;
